﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShot : MonoBehaviour
{
    public GameObject bulletPrefab;

    public List<Transform> enemiesToShot;

    public float timeLastShot;

    public float shotFrequency;
    // Update is called once per frame
    void Update()
    {
        if (enemiesToShot != null)
        {
            if (Time,time - timeLastShot)
            {
                GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
                bullet.GetComponent<BulletMovement>().enemy = enemyToShot[0];
                timeLastShot = Time.time;
            }
        }
    }
}
